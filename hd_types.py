"""
Classes representing special data objects for main HelpDesk class.

These are copied as new classes via the type() function, such that when
HelpDesk() is instantiated, it will hold unique classes with the hd
attribute pre-set (ticket objects work in a similar way, using special
types from tk_types.py). That means these classes, in 99% of situations,
should never be directly instantiated. It is better to use it as
follows:

myhd = HelpDesk(API_KEY,BASE_URL)
myTicket = myhd.Ticket() # or myhd.User(), etc

You can re-parent an object to another HelpDesk instance by changing
the hd attribute, but make sure you know what you're doing....

This also means when extending these classes, it should either be done
prior to instantiation of a HelpDesk object, OR by extending the
HelpDesk instance's copy.

This may be a slightly unusual approach, but it makes for a very
intuitive API in the end, save for a couple of edge cases. I will
re-examine my approach, should I get significant feedback leading to a
better way.
"""
import pfdef,tk_types
from bases import DictBase,ListBase,set_attrs
from endpoints import *

class Agent(DictBase):
    """
    An object representing a FD/FS agent
    """
    def __init__(self,data=pfdef.agent,helpdesk=None):
        super(self.__class__,self).__init__(
            data,helpdesk,sf={'user':self.hd.User}
        )

        if helpdesk:
            helpdesk.agents.append(self)


class Agents(ListBase):
    """
    List of all agents for a given HelpDesk object. Mainly used by
    HelpDesk.agents property.

    NOTE: The HelpDesk class caches agents the first time the
    self.agents() property is used. The cache can be refreshed via
    self.refresh_agents()

    TO-DO:
     - Get all pages
     - Can we add agents or update infos?
    """
    def __init__(self,data=[],helpdesk=None):
        super(self.__class__,self).__init__(
            data,helpdesk,it=self.hd.Agent
        )

        if helpdesk:
            helpdesk.agents += self


    def __getitem__(self,item):
        """
        Finds an FD/FS agent by full name or user_id. Returns a
        HelpDesk.Agent object or None.

        Arguments:
        item (str/unicode) - An agent's full name or user_id
        """
        for agent in self:
            if agent.user_id == item or agent.user.name == item:
                return agent

    def name_from_id(self,user_id):
        """
        Returns the full name of an agent of a given user_id, or
        None if no such agent exists

        Arguments:
        user_id (int) - An FD/FS user_id to retrieve
        """
        for agent in self:
            if agent.user_id == user_id:
                return agent.user.name

    def id_from_name(self,name):
        """
        Returns the user_id of a given agent name, or None if no
        such agent exists.

        Arguments:
        name (str,unicode) - An FD/FS agent's name to retrieve
        (usually First and Last names, in that order, space
        separated)
        """
        for agent in self.__agents:
            if agent.user.name == name:
                return agent.user_id


class Tickets(ListBase):
    """
    Currently a convenience class - not really used by the API,
    as of pre-alpha-7. Useful to organize groups of tickets you
    download or create, keeping them organized with names and types
    (e.g. you may need to check that mytickets is a bunch of
    tickets, so rather than using all(), you could just put them
    in a Tickets object and just check the type of mytickets)
    """
    def __init__(self,data=[],helpdesk=None):
        super(self.__class__,self).__init__(data,it=self.hd.Ticket)


class Ticket(DictBase):
    """
    An object represetning an FD/FS ticket.

    Known FD/FS Ticket properties:
    %s

    TO-DO:
     - Implement updating tickets on FD/FS
     - Implement creating new tickets

    Arguments:
    parent=HelpDesk('','') (HelpDesk) - A HelpDesk object we
    belong to.

    data (dict) - A dict (or dict-like, e.g. another Ticket) object
    to use as initial data. If no data is given, all ticket fields
    are filled in from pfdef cache.
    """ % ',\n'.join(pfdef.ticket.keys())

    def __init__(self,data=pfdef.ticket,helpdesk=None,):
        hdt = 'helpdesk_ticket'

        t = ( (i,getattr(tk_types,i))
            for i in dir(tk_types) if i not in ('DictBase','ListBase')
        )

        for k,v in t:
            if type(v) is type:
                setattr( self,k,type(k,v.__bases__,dict(v.__dict__)) )
                v = getattr(self,k)
                v.tk = self

        sf = {
            'notes':self.Notes,
            'attachments':self.Attachments,
            'custom_field':self.Custom_Field,
            'cc_email':self.Cc_Email,
            'tags':self.Tags
        }

        self.__parents = None
        self.__children = None

        super(self.__class__,self).__init__(data,helpdesk,sf=sf)

        if not self.created_at:
            # Maybe default should be something more
            # blank/empty...
            self.created_at = datetime.strftime(NOW(),DTFMT)

    @property
    def merged_parents(self):
        """
        Extracts the ticket numbers from notes that start with "This
        ticket is closed and merged into", and returns an hd.Tickets
        object.

        Fills self.__parents so it only has to do the search and API
        calls once.
        """
        if not self.__parents:
            self.__parents = self.hd.Tickets([
                self.hd.tickets[int(n.body.split()[-1].rstrip('.'))]
                for n in self.notes
                if n.body.startswith(pfdef.CAM_TEXT)
            ])
        return self.__parents

    @property
    def merged_children(self):
        """
        Extracts the ticket numbers from notes that start with "Merged
        from ticket", and returns an hd.Tickets object.

        Fills self.__children so it only has to do the search and API
        calls once.
        """
        if not self.__children:
            self.__children = self.hd.Tickets([
                self.hd.tickets[
                    int(n.body.split(pfdef.MFT_TEXT)[1].split()[0])
                ]
                for n in self.notes
                if n.body.startswith(pfdef.MFT_TEXT)
            ])
        return self.__children

class User(DictBase):
    """
    An object representing a FD/FS User

    TO-DO:
     - Can we add or modify users?
     - Besides HelpDesk.Agent, could other FD/FS objects make
     use of this class?
    """
    def __init__(self,data=pfdef.user,helpdesk=None):
        super(self.__class__,self).__init__(data,helpdesk)
