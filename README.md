## DEAD ##
This project is dead. I will be starting from scratch on a new module to support Freshdesk's new V2 API. UPDATE: as of 2017 this is still planned but not started.

### Description
PyFreshdesk - A Python interface to the Freshdesk/Freshservice API.

### License:
Copyright 2015 Michal Kloko All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

### WARNING:
This project, like all of my others, is in EXTREMELY early stages of development and probably not useful to anyone else, beyond example material.

Currently can only get and represent endpoints, in a somewhat finalized way. There is currently NO put/post suppoert (you cannot update or create new FD/FS endpoints).

### Description:
A clean, hyper intuitive Python interface to the Freshdesk/Freshservice (hereto referred to as FD/FS) restful API.

Much of this API is just a trick of namespaces - an optical illusion of code, if you will. We get "endpoints" from a FD/FS "BASE_URL" (the URL to your helpdesk), convert the returned JSON data into built-in Pyhton objects (via the json module loads() function), and finally turn those objects into custom objects with discrete types, attributes, and methods.

API reference is provided via docstrings

Some examples:
```
#!python
# There are a two ways to import pyfreshdesk, outlined here. import *
# is not supported.
#
# Any time you have set an API_KEY and BASE_URL in config.ini, a default
# HelpDesk object will be created at pyfreshdesk.hd
#
# Tip: review templates in pfdef, for attributes of PyFD classes
#
# Warning: this code is untested/checked for syntax errors or other
# issues. Useful only to give a basic understanding of the API.

# Import method 1 (single-instance/module mode, which assumes you've set
# API_KEY and BASE_URL in config.ini):
from pyfreshdesk import hd

# Get a ticket, and do something...
for n in hd.tickets[14920].notes:
    print n.body

# Iterate over a range of tickets...
for ticket in hd.tickets[20120:20125]:
    print ticket.description

# Why we labor over all those classes:
def update_description(ticket,data):
    """Appends data to ticket description (note that as of this
    wiriting, no write functionality exists in PyFreshdesk)"""
    assert type(ticket) is hd.Ticket

    ticket.description = "%s\n%s" % (ticket.description,data)

# Iterate over open tickets...see keys of endpoints.TICKETS for more.
for ticket in hd.tickets.open:
    update_description(ticket,'All your base are belong to us.')

# Print agent names...
for agent in hd.agents:
    print agent.user.name

# Import method 2 (multi-instance mode for working with multiple FD/FS
# helpdesks at the same time, in the same app):
import pyfreshdesk
hd = pyfreshdesk.HelpDesk('API_KEY1','BASE_URL1')
```