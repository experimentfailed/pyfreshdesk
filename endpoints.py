#!/usr/bin/python2.7
"""
PyFreshdesk Endpoints...KEEP DICT NAMES UNIQUE (because import *)
"""

__h = 'helpdesk/%s'
__ht = __h % 'tickets/%s'
__htf = __ht % 'filter/%s'

TICKET = {
    'new':__h % 'tickets',
    'single':__ht,
}

# Used by HelpDesk.__Tickets - can access any of these filters like,
# e.g.: myhd.tickets.on_hold
TICKETS = {
    'all':__htf % 'all_tickets',
    'open':__htf % 'open',
    'overdue':__htf % 'overdue',
    'on_hold':__htf % 'on_hold',
    'unresolved':__htf % 'unresolved',
    'due_today': __htf % 'due_today',
    'unassigned': __htf % 'new'
}

CONTACT = {
    'single':'contacts/%s',
}

AGENTS = {
    'all':'agents',
}
