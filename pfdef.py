"""
Some data & caching for PyFreshdesk:
Key/Type templates for HelpDesk inner classes (e.g. HelpDesk.Ticket,
HelpDesk.Agent, HelpDesk.Ticket.Note, etc). We'll fall back on these, if
there are no cached API types.

Probably we'll put the cache handler in here...The idea is basically any
time HelpDesk.get_ticket is called, we'll generate dictionaries like
these and pickle them. Thus, whenever creating a new special PyFreshdesk
type/object, it will be filled with the correct/latest attributes,
keeping the entire API always consistent with FD/FS. This will be
especially nice for custom fields.
"""

# Closed and merged string to check for in notes (startswith)
CAM_TEXT = 'This ticket is closed and merged'
MFT_TEXT = 'Merged from ticket'

ticket = {
    'helpdesk_ticket':{
        'status':int(),
        'source_name':unicode(),
        'attachments':list(),
        'frDueBy':unicode(), # dt
        'ticket_type':unicode(),
        'updated_at':unicode(), # dt
        'urgent':bool(),
        'trained':bool(),
        'email_config_id':int(),
        'id':int(),
        'subject':unicode(),
        'description_html':unicode(),
        'isescalated':bool(),
        'due_by':unicode(), # dt
        'custom_field':dict(),
        'responder_id':int(),
        'priority_name':unicode(),
        'priority':int(),
        'source':int(),
        'cc_email':dict(),
        'owner_id':bool(),
        'display_id':int(),
        'requester_name':unicode(),
        'to_emails':list(), # A list of email addresses
        'description':unicode(),
        'tags':list(),
        'deleted':bool(),
        'requester_status_name':unicode(),
        'delta':bool(),
        'requester_id':int(),
        'responder_name':unicode(), # Assigned agent name
        'to_email':unicode(),
        'product_id':None, # Not sure?
        'status_name':unicode(),
        'fr_escalated':bool(),
        'created_at':unicode(), # dt
        'spam':bool(),
        'group_id':int(),
        'notes':list()
    }
}

attachment = {
    'attachment_url':unicode(),
    'content_file_size':int(),
    'created_at':unicode(), # dt
    'updated_at':unicode(), # dt
    'content_file_name':unicode(),
    'content_content_type':unicode(),
    'id':int()
}

tag = {'name':unicode()}

note = {
    'note':{
        'body':unicode(),
        'incoming':bool(),
        'attachments':list(),
        'deleted':bool(),
        'created_at':unicode(), #dt
        'body_html':unicode(),
        'private':bool(),
        'updated_at':unicode(), #dt
        'source':int(),
        'user_id':int(),
        'support_email':None, #?
        'id':int()
    }
}

cc_email = {
    # Each is a list of strings of email addresses
    'cc_emails':list(),
    'reply_cc':list(),
    'fwd_emails':list()
}

# To-do
agent = {}
user = {}
