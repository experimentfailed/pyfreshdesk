"""
Description: A Python interface to the Freshdesk/Freshservice (hereto
referred to as FD/FS) API.

Status:
Pre-Alpha - Nothing is considered "stable", or in any way final. This
API is prone to change dramatically during this phase of development.

Required third-party modules:
 - requests
 - requests[security]

Please see README.md for more information & usage examples.
"""
#~ Copyright 2015-2016 Michal Kloko All rights reserved.
#~
#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or (at
#~ your option) any later version.
#~
#~ This program is distributed in the hope that it will be useful, but
#~ WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#~ General Public License for more details.
#~
#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see http://www.gnu.org/licenses/.

version = 'pre-alpha-9'

import json,requests,ConfigParser,hd_types
from bases import TicketHandler
from urllib2 import urlopen
from datetime import datetime,timedelta
import logging as log
from endpoints import AGENTS

log.getLogger("requests").setLevel(log.WARNING) # idr what this is for

cfg = ConfigParser.ConfigParser()
cfg.read('/usr/lib/python2.7/pyfreshdesk/config.ini')

BATCH_CALL_RESERVE = cfg.get(
    'API_CALL_TRACKING','BATCH_CALL_RESERVE'
) or 250

CALL_LIMIT_WARN = cfg.get('API_CALL_TRACKING','CALL_LIMIT_WARN') or 800
API_CALL_LIMIT = 1000

API_KEY = cfg.get('HELPDESK','API_KEY') or ''
if API_KEY == 'None': API_KEY = ''
BASE_URL = cfg.get('HELPDESK','BASE_URL') or ''
if BASE_URL == 'None': BASE_URL = ''

# Date/time format as presented by FD/FS (timezone not implemented yet)
DTFMT = '%Y-%m-%dT%H:%M:%S' # We can probably pfdef this

NOW = datetime.now
ONE_HOUR = timedelta(hours=1)
START_TIME = THIS_HOUR_START = NOW()

REQUESTS_THIS_HOUR = TOTAL_REQUESTS = 0

def track_requests():
    """
    Used by HelpDesk.get_endpoint() to track requests-per-hour and stop
    trying after limit reached. Freshdesk limits us to 1000 requests
    per hour. However, if for some reason you need to make manual
    requests (not using the HelpDesk method) in your script, you may
    want to call this to keep track.

    TO-DO:
    - Re-think this whole thing...
    - Implement throttling and batch operations
    """
    global TOTAL_REQUESTS, THIS_HOUR_START, REQUESTS_THIS_HOUR
    TOTAL_REQUESTS += 1

    elapsed = NOW() - THIS_HOUR_START

    if elapsed >= ONE_HOUR:
        THIS_HOUR_START = NOW()
        REQUESTS_THIS_HOUR = 0
        return True
    else:
        REQUESTS_THIS_HOUR += 1

    if elapsed < ONE_HOUR and REQUESTS_THIS_HOUR >= API_CALL_LIMIT:
        print "WARNING: Exceeded requests-per-hour limit!"
        return False
    elif elapsed < ONE_HOUR and REQUESTS_THIS_HOUR >= CALL_LIMIT_WARN:
        print "WARNING: Approaching max requests-per-hour!"
        return True
    else:
        return True


class HelpDesk(object):
    """
    Main FD/FS interface

    Arguments:
    api_key (str) - Your Freshdesk API key

    base_url (str) - Your Freshdesk API endpoint (e.g.
    https://myhelpdesk.freshdesk.com/helpdesk/)

    debug=False (bool) - Doesn't usually do much. What it does do
    changes a lot, and in general could break things.
    """
    def __init__(self,api_key,base_url,debug=False):
        self.tickets = TicketHandler(self)

        # Here we make copies of all the classes in hd_types.py so
        # that all of the objects spawned by an instance of this class
        # will have thier hd attribute pre-set properly
        t = ( (i,getattr(hd_types,i))
            for i in dir(hd_types) if i not in ('DictBase','ListBase')
        )

        for k,v in t:
            if type(v) is type:
                setattr( self,k,type(k,v.__bases__,dict(v.__dict__)) )
                v = getattr(self,k)
                v.hd = self

        self.API_KEY = api_key # An FD/FS API key
        self.BASE_URL = base_url.rstrip('/') # Base endpoint
        self.TOTAL_REQUESTS = 0 # Total requests of this instance
        self.debug = debug
        self.__agents = None

    def User(self,data={}):
        return types.User(data,self)

    @property
    def agents(self):
        """
        Returns an Agents object. Stores results to reduce API calls.
        """
        if not self.__agents:
            self.refresh_agents()
        return self.__agents

    @agents.setter
    def agents(self,data):
        assert type(data) in (self.Agents,self.Agent)
        if type(data) is self.Agent:
            self.__agents.append(data)
        else:
            self.__agents += data

    def refresh_agents(self):
        """
        Refresh the agents cache

        NOTE: Makes 1 FD/FS API call per page of agents.
        """
        self.__agents = self.Agents( self.get_pages(AGENTS['all']) )

    def get_pages(self,ep):
        """
        Yields each item in each page at a given FD/FS multi-page
        endpoint

        NOTE: FD/FS returns 30 items per page

        Arguments:
        ep (str,unicode) - An FD/FS API endpoint
        """
        page_num = 0

        while True:
            page = self.get_endpoint(ep,page_num+1)

            if type(page) is list:
                if len(page) >= 1:
                    for i in page:
                        yield i
                else:
                    break
            else:
                raise ValueError('Invalid endpoint:',TICKETS[ep])

            page_num += 1

    def convert_dt(self,dt,DTFMT=DTFMT):
        """
        If a datetime object is passed, returns a formatted string akin
        to how FD/FS formats their date/time stamps. Otherwise, converts
        a date/time stamp string from FD/FS to a proper Python datetime
        object.

        NOTE: Timezone not implemented yet!

        Example FD/FS timestamp: 2016-04-01T17:28:04-04:00

        Arguments:
        dt (str,unicode,datetime) - A date/time stamp to convert
        """
        if type(dt) is datetime:
            return datetime.strftime(dt,DTFMT)
        #~ tz = s[19:]
        return datetime.strptime(dt[0:19],DTFMT)

    def __prep_ep(self,ep,page=None):
        """
        Tries to correctly format & return an endpoint URL string
        """

        ep = '%s/%s.json%s' % (
            self.BASE_URL,ep.lstrip('/'),
            '?page=%s' % page if page else ''
        )

        if self.debug: print '__prep_ep:',ep

        return ep

    def get_endpoint(self,endpoint,page=None):
        """
        Performs a GET request of a given FD/FS endpoint, as defined in
        endpoints.py. Returns a json.loads() objuect (usually a dict).

        Arguments:
        endpoint (str) - An endpoint as defined in endpoints.py

        page (int) - A page number to get at the given endpoint
        """
        self.TOTAL_REQUESTS += 1

        endpoint = self.__prep_ep(endpoint,page)

        if self.debug: print endpoint

        if track_requests():
            request = requests.get( endpoint,auth=(self.API_KEY, "X") )

            return json.loads(request.content)

    def create_endpoint(self,endpoint,payload):
        """
        Performs a POST request of a given payload, to a given endpoint.

        Warning: untested.

        Arguments:
        endpoint (str,unicode) - An FD/FS endpoint to POST to.

        payload (any) - An object that can be converted by json.dumps().
        Eventually, this will be detected and handled accordingly (i.e.
        you could pass a string of a json.dumps() or a json.dumps-able
        object)
        """
        self.TOTAL_REQUESTS += 1

        if track_requests():
            return requests.post(
                self.__prep_ep(endpoint),
                headers = {'Content-Type': 'application/json'},
                data=json.dumps(payload),
                allow_redirects=True,
                auth=(self.API_KEY, "X")
            )
        else:
            print "Request was not processed!"
            return False

    def update_endpoint(self,endpoint,payload):
        """
        Performs a PUT request of a given payload, to a given endpoint

        Warning: untested.

        Arguments:
        endpoint (str) - A path to the final API endpoint. NOTE: On a
        quick web search, it seems that "method" might be a better name
        for this argument.

        payload (any) - An object that can be converted by json.dumps().
        Eventually, this will be detected and handled accordingly (i.e.
        you could pass a string of a json.dumps() or a json.dumps-able
        object)
        """
        self.TOTAL_REQUESTS += 1

        if track_requests():
            return requests.put(
                self.__prep_ep(endpoint),
                headers = {'Content-Type': 'application/json'},
                data=json.dumps(payload),
                allow_redirects=True,
                auth=(self.API_KEY, "X")
            )
        else:
            print "Request was not processed!"
            return False


if API_KEY and BASE_URL:
    hd = HelpDesk(API_KEY,BASE_URL)
