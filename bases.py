from endpoints import *
import json

def set_attrs(items,attr,value):
    """
    Sets attr of each item in items to value. Used by HelpDesk.__init__
    and HelpDesk.Ticket.__init__
    """
    for i in items:
        setattr(i,attr,value)


class DictBase(dict):
    """
    Intended to be inherited and mainly used by classes within the main
    HelpDesk class. In short, this is an extended dict with getattr &
    setattr support, along with some JSON capabilities, allowing access
    to dict keys like a normal class attribute (this is the 'magic' of
    this API design)

    Warning:
    Due to the get/setattr support, if you try to add an attribute that
    is the same name as a key in the data you instantiated with, it will
    set self[key] instead of adding a new attribute. Also, you cannot
    add keys to the dict via setattr, and you should avoid doing so
    either way, so as to preserve the JSON output for writing back to
    FD/FS.

    Arguments:
    data (dict) - A dict or dict-like object to initialize with

    sf (dict) - A dict of special ticket fields (keys of type str) and
    classes that they should instantiate (values of class names). To see
    an example, have a look at the the code in:
    PyFreshdesk.HelpDesk.Ticket.__init__.
    """

    hd = None
    tk = None

    def __init__(self,da={},hd=None,tk=None,sf={}):
        self.hd = hd or self.hd
        self.tk = tk or self.tk

        sf = sf if type(sf) is dict else {}

        d = da[da.keys()[0]] if len(da) is 1 else da

        if type(d) is dict: d["changes"] = []

        for k in d:
            if k in sf:
                d[k] = sf[k](d[k],self)

        super(DictBase,self).__init__(da)

    def __getattr__(self,item):
        """
        Tries to get the value of item as a key in self; else tries to
        gets the attribute.
        """
        if len(self) is 1 and item in self[self.keys()[0]]:
            return self[self.keys()[0]][item]

        if item in self:
            return self[item]

        return super(DictBase,self).__getattribute__(item)

    def __setattr__(self,item,value):
        """
        Sets self[item] to value, if item is a key in self; else adds a
        new attribute to the instance, setting it to value.
        """
        if len(self) == 1 and item in self[self.keys()[0]]:
            self[self.keys()[0]][item] = value
            if item not in self[self.keys()[0]]["changes"]:
                self[self.keys()[0]]["changes"].append(item)
        elif item in self:
            self[item] = value
            if item not in self["changes"]:
                self["changes"].append(item)
        else:
            super(DictBase,self).__setattr__(item,value)

    def json_ready(self,w=False):
        """
        Returns a dict for json.dumps(). See self.json() method.
        """
        if not w:
            return {k:(v.json_ready() if hasattr(v,'json_ready') else v)
                for k,v in self.items()
            }

        d = self[self.keys()[0]] if len(self) is 1 else self

        out = {k:(v.json_ready(True) if hasattr(v,'json_ready') else v)
            for k,v in d.items()
            if k in self.__getattr__("changes")
        }

        if len(self) is 1:
            out = {self.keys()[0]:out}

        return out

    def json(self,w = False):
        """
        Returns a json string, resembling the origonal data sent by
        FD/FS. Should be overriden in subclasses to return appropriate
        data for put/post requests.
        
        Pass True to get only changes for writing back to FD. This is
        incomplete, as it does not consider what fields FD will actually
        let us write to, so when changing a ticket for example, you will
        have to manually discover which fields are writeable. Also, some
        fields, such as Notes have their own endpoint through which they
        are manipulated. None of this is hashed out in this module
        currently.
        
        Example:
        ticket = myhd.tickets[1234]
        ticket.status = 3
        print ticket.json(True)
        
        Would output:
        {"helpdesk_ticket": {"status": 3}}
        """
        return json.dumps( self.json_ready(w) )


class ListBase(list):
    """
    Intended to be inherited; mainly used by classes within HelpDesk.
    As of pre-alpha-5, it offers no special capabilities.
    """

    hd = None
    tk = None

    # da=data, it=item type, hd=HelpDesk, tk=Ticket, ia=init action
    def __init__(self,da=[],hd=None,tk=None,it=None,ia=None):
        self.hd = hd or self.hd
        self.tk = tk or self.tk

        if it: da = (it(i) for i in da)

        super(ListBase,self).__init__(da)

        if hd and ia: ia += self

    def __add__(self,other):
        assert type(other) == type(self)

        return getattr(self.hd,type(other).__name__)(
            super(ListBase,self).__add__(other)
        )

    __radd__ = __add__


class TicketHandler(object):
    """
    The HelpDesk.tickets object - mainly for name's sake - provides
    a nice interface for getting tickets, and ticket pages.

    Arguments:
    helpdesk (HelpDesk) - A HelpDesk object to use
    """
    def __init__(self,helpdesk):
        self.hd = helpdesk

        # Auto-set properties for each pfdef.TICKETS endpoint
        for f in TICKETS:
            doc = "Yields each %s ticket" % f
            setattr(
                self.__class__, f, property(
                    lambda self,i=f: self.__pages(i),
                    doc=doc
                )
            )

    def __getitem__(self,item):
        """
        Returns a HelpDesk.Ticket object of the specified ticket
        (e.g. myhd.tickets[5382])
        """
        return self.hd.Ticket(
            self.hd.get_endpoint(TICKET['single'] % int(item))
        )

    def __getslice__(self,start,end):
        """
        Yields a HelpDesk.Ticket object for each ticket in the
        specified range, e.g. myhd.tickets[14920:14925]
        """
        for t in xrange(start,end+1):
            yield self[t]

    @property
    def latest(self):
        """
        Returns a HelpDesk.Ticket object of the ticket with the
        highest display_id (aka ticket number)
        """
        for t in self.hd.tickets.__pages():
            return t

    def __pages(self,ep):
        """
        Yields a HelpDesk.Ticket object for each ticket on each page
        of the specified range.

        Arguments:
        ep (str,unicode) - An endpoint from which to retrieve pages
        of tickets
        """
        for t in self.hd.get_pages(TICKETS[ep]):
            yield self.hd.Ticket(t)
