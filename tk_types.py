"""
Special ListBase & DictBase objects used by hd_types.Ticket
"""
import pfdef
from bases import DictBase,ListBase

class Tags(ListBase):
    def __init__(self,data=[],helpdesk=None,ticket=None):
        super(self.__class__,self).__init__(
            data,helpdesk,it=self.tk.Tag,tk=ticket
        )

        if ticket:
            ticket.tags += self


class Tag(DictBase):
    def __init__(self,data=pfdef.tag,ticket=None):
        super(self.__class__,self).__init__(data or pfdef.tag,tk=ticket)

        if ticket:
            ticket.tags.append(self)


class Custom_Field(DictBase):
    def __init__(self,data={},ticket=None):
        super(self.__class__,self).__init__(
            data or pfdef.custom_field,tk=ticket
        )

        if ticket:
            ticket.custom_field = self

    #~ def __getattr__(self,item):
        #~ tail = item.split('_')[-1]
        #~ if tail.isdigit() and len(tail) == 6:
            #~ super(Custom_Field,self).__getitem__(item)
        #~ digits = '_%s' % self.keys()[0].split('_')[-1]

class Cc_Email(DictBase):
    def __init__(self,data=pfdef.cc_email,ticket=None):
        super(self.__class__,self).__init__(
            data or pfdef.cc_email,tk=ticket
        )

        if ticket:
            ticket.cc_email = self


class Attachment(DictBase):
    def __init__(self,data=pfdef.attachment,ticket=None):
        super(self.__class__,self).__init__(
            data or pfdef.attachment,tk=ticket
        )

        if ticket:
            ticket.attachments.append(self)


class Attachments(ListBase):
    def __init__(self,data=[],ticket=None):
        super(self.__class__,self).__init__(
            data,it=self.tk.Attachment,tk=ticket
        )

        if ticket:
            ticket.attachments += self


class Note(DictBase):
    def __init__(self,data=pfdef.note,ticket=None):
        super(self.__class__,self).__init__(
            data or pfdef.note,tk=ticket
        )

        if ticket:
            ticket.notes.append(self)

        ###########
        ########### Investigate incomming keys to see if we can
        # use an existing attachment object
        #~ self['attachments'] = HelpDesk.Ticket.Attachments(
            #~ self['attachments']
        #~ )
        ###########
        ###########


class Notes(ListBase):
    """
    An object representing a list of ticket notes. Methods such
    as __iter__ and __len__ will return only non-deleted notes.
    Use special methods e.g. deleted() and all() to work with
    deleted notes.
    """
    def __init__(self,data=[],ticket=None):
        super(self.__class__,self).__init__(
            data,it=self.tk.Note,tk=ticket
        )

        if ticket:
            ticket.notes += self
